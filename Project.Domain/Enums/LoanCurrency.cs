namespace Project.Domain.Enums;

public enum LoanCurrency
{
    Gel = 1,
    Usd = 2,
    Eur = 3
}