namespace Project.Domain.Enums;

public enum LoanType
{
    AutoLoan = 1,
    Installment = 2,
    TurboLoan = 3
}