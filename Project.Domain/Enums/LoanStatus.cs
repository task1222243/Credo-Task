namespace Project.Domain.Enums;

public enum LoanStatus
{
    Forwarded = 1,
    UnderProcessing = 2,
    Approved = 3,
    Rejected = 4
}