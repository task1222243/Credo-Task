namespace Project.Domain.Entities;

public sealed partial class User
{
    public int Id { get; set; }
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string PhoneNumber { get; set; } = null!;
    public string PersonalNumber { get; set; } = null!;
    public string PasswordHash { get; set; } = null!;
    public DateOnly DateOfBirth { get; set; }
    public DateTime RegistrationDate { get; set; }
    public List<LoanApplication>? Loans { get; set; }


    public User()
    {
    }

    public User(
        string firstName,
        string lastName,
        string phoneNumber,
        string personalNumber,
        DateOnly dateOnly)
    {
        FirstName = firstName;
        LastName = lastName;
        PhoneNumber = phoneNumber;
        PersonalNumber = personalNumber;
        DateOfBirth = dateOnly;
        RegistrationDate = DateTime.UtcNow;
    }
}