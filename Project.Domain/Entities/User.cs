namespace Project.Domain.Entities;

public sealed partial class User
{
    public void SetPassword(string hashedPassword)
    {
        PasswordHash = hashedPassword;
    }

    public void AddLoanApplication(LoanApplication application)
    {
        Loans?.Add(application);
    }
}