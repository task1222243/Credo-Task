using System.Text.Json.Serialization;
using Project.Domain.Enums;

namespace Project.Domain.Entities;

[Serializable]
public sealed class LoanApplication
{
    public int Id { get; set; }
    public int UserId { get; set; }
    
    [JsonIgnore]
    public User? User { get; set; }
    public decimal Amount { get; set; }
    public LoanType LoanType { get; set; }
    public LoanCurrency LoanCurrency { get; set; }
    public LoanStatus LoanStatus { get; set; }
    public int LoanPeriodInMonth { get; set; }
    

    public LoanApplication(
        int userId,
        decimal amount,
        LoanType loanType,
        LoanCurrency loanCurrency,
        int loanPeriodInMonth)
    {
        UserId = userId;
        Amount = amount;
        LoanType = loanType;
        LoanCurrency = loanCurrency;
        LoanPeriodInMonth = loanPeriodInMonth;
    }


    public void UpdateLoanApplication(decimal amount, LoanType loanType, LoanCurrency loanCurrency)
    {
        LoanCurrency = LoanCurrency == loanCurrency ? LoanCurrency : loanCurrency;
        LoanType = LoanType == loanType ? LoanType : loanType;
        Amount = Amount == amount ? Amount : amount;
    }
}