using Microsoft.AspNetCore.Identity;

namespace Project.Application.Interfaces;

public interface ICustomPasswordHasher
{
    string HashPassword(string password);

    PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword);
}