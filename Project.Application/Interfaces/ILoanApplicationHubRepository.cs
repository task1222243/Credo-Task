using Project.Domain.Entities;

namespace Project.Application.Interfaces;

public interface ILoanApplicationHubRepository
{
    Task<int> CreateLoanApplicationAsync(LoanApplication application);

    Task<List<LoanApplication>> GetLoanApplicationsByUserId(int userId);

    Task<LoanApplication?> GetLoanApplicationByLoanId(int loanId);

    Task<LoanApplication> UpdateLoanApplication(LoanApplication loanApplication);

    Task<int> DeleteLoanApplication(LoanApplication loanApplication);
}