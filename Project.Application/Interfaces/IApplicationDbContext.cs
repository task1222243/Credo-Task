using Microsoft.EntityFrameworkCore;
using Project.Domain.Entities;

namespace Project.Application.Interfaces;

public interface IApplicationDbContext
{
    DbSet<User> Users { get; }

    DbSet<LoanApplication> LoanApplicationHub { get; }
}