using Project.Domain.Entities;

namespace Project.Application.Interfaces;

public interface IUserRepository
{
    Task<User?> GetUserByIdAsync(int userId);

    Task<int> AddUserAsync(User user);

    Task<User> UpdateUserAsync(User user);

    Task<User?> GetUserByPhoneNumber(string phoneNumber);
    
    Task<List<User>> GetUsers(Func<User?, bool> filter);
}