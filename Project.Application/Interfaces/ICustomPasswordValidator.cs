using Microsoft.AspNetCore.Identity;

namespace Project.Application.Interfaces;

public interface ICustomPasswordValidator
{
    public Task<IdentityResult> ValidateAsync(string password);
}