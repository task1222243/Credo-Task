using Project.Application.Users.Queries.QueryModels;
using Project.Domain.Entities;

namespace Project.Application.Common.Extensions;

public static class CommonExtensions
{
    public static UserDto ToUserDto(this User user)
    {
        return new UserDto(
            user.Id,
            user.FirstName,
            user.LastName,
            user.PhoneNumber,
            user.PersonalNumber,
            user.DateOfBirth,
            user.RegistrationDate,
            user.Loans?.Select(o => o.ToLoanApplicationDto()).ToList()!
        );
    }

    public static LoanApplicationDto ToLoanApplicationDto(this LoanApplication loanApplication)
    {
        return new LoanApplicationDto(
            loanApplication.Id,
            loanApplication.Amount,
            loanApplication.LoanType,
            loanApplication.LoanCurrency,
            loanApplication.LoanStatus,
            loanApplication.LoanPeriodInMonth);
    }
}