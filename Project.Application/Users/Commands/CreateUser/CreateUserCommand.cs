using MediatR;

namespace Project.Application.Users.Commands.CreateUser;


[Serializable]
public sealed class CreateUserCommand : IRequest<int>
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public string PersonalNumber { get; set; }
    public string Password { get; set; }
    public DateOnly DateOfBirth { get; set; }
    
    public CreateUserCommand(
        string firstName,
        string lastName, 
        string phoneNumber,
        string personalNumber, 
        string password, 
        DateOnly dateOfBirth)
    {
        FirstName = firstName;
        LastName = lastName;
        PhoneNumber = phoneNumber;
        PersonalNumber = personalNumber;
        Password = password;
        DateOfBirth = dateOfBirth;
    }
}