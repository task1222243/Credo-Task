using MediatR;
using Microsoft.Extensions.Logging;
using Project.Application.Exceptions;
using Project.Application.Interfaces;
using Project.Domain.Entities;

namespace Project.Application.Users.Commands.CreateUser;

public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, int>
{
    private readonly IUserRepository _userRepository;

    private readonly ICustomPasswordHasher _passwordHasher;

    private readonly ILogger<CreateUserCommandHandler> _logger;

    public CreateUserCommandHandler(
        IUserRepository userRepository,
        ICustomPasswordHasher passwordHasher,
        ILogger<CreateUserCommandHandler> logger)
    {
        _logger = logger;
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
    }

    public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        try
        {
            await CheckPersonalNumberAsync(request);
            
            await CheckPhoneNumberAsync(request);
            
            var user = new User(
                request.FirstName,
                request.LastName,
                request.PhoneNumber,
                request.PersonalNumber,
                request.DateOfBirth);

            var hashedPassword = _passwordHasher.HashPassword(request.Password);

            user.SetPassword(hashedPassword);

            await _userRepository.AddUserAsync(user);

            _logger.LogInformation($"User created {user.PhoneNumber}");

            return user.Id;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
    }

    private async Task CheckPhoneNumberAsync(CreateUserCommand request)
    {
        if ((await _userRepository.GetUsers(o => o?.PhoneNumber == request.PhoneNumber)).Any())
        {
            throw new ValueInUseException($"Phone number in use {request.PhoneNumber}");
        }
    }

    private async Task CheckPersonalNumberAsync(CreateUserCommand request)
    {
        if ((await _userRepository.GetUsers(o => o?.PersonalNumber == request.PersonalNumber)).Any())
        {
            throw new ValueInUseException($"Personal number in use {request.PersonalNumber}");
        }
    }
}