using FluentValidation;

namespace Project.Application.Users.Commands.CreateUser;

public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
{
    public CreateUserCommandValidator()
    {
        RuleFor(command => command.FirstName).NotEmpty();
        RuleFor(command => command.LastName).NotEmpty();
        RuleFor(command => command.PhoneNumber).NotEmpty()
            .Must(o => o.Length == 9)
            .WithMessage("phone number must be equal 9 digits");
        RuleFor(command => command.PersonalNumber).NotEmpty()
            .Must(o => o.Length == 11)
            .WithMessage("Personal number must be equal 11 digits");
        RuleFor(command => command.Password).NotEmpty()
            .Must(o => o.Length >= 8)
            .WithMessage("Password length must be greater or equal 8");
        RuleFor(command => command.DateOfBirth).NotEmpty();
    }
}