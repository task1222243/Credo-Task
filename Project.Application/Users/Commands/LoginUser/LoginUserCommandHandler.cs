using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Project.Application.Exceptions;
using Project.Application.Interfaces;
using Project.Domain.Entities;

using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace Project.Application.Users.Commands.LoginUser;

public class LoginUserCommandHandler : IRequestHandler<LoginUserCommand, string>
{
    private readonly ICustomPasswordHasher _passwordHasher;
    private readonly IUserRepository _userRepository;
    private readonly IConfiguration _configuration;
    private readonly ILogger<LoginUserCommandHandler> _logger;

    public LoginUserCommandHandler(
        ICustomPasswordHasher passwordHasher,
        IUserRepository userRepository, 
        IConfiguration configuration,
        ILogger<LoginUserCommandHandler> logger)
    {
        _passwordHasher = passwordHasher;
        _userRepository = userRepository;
        _configuration = configuration;
        _logger = logger;
    }

    public async Task<string> Handle(LoginUserCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _userRepository.GetUserByPhoneNumber(request.PhoneNumber) 
                       ?? throw new NotFoundException("PhoneNumber or Password is incorrect");

            var passwordValidationResult = _passwordHasher.VerifyHashedPassword(user.PasswordHash, request.Password);

            if (passwordValidationResult == PasswordVerificationResult.Failed)
            {
                throw new NotFoundException("PhoneNumber or Password is incorrect");
            }
        
            return GenerateJwtToken(user);

        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
        
    }

    private string GenerateJwtToken(User user)
    {
        var authClaims = new List<Claim>
        {
            new(ClaimTypes.MobilePhone, user.PhoneNumber),
            new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        };

        var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]!));


        var result = new JwtSecurityToken(
            issuer: _configuration["JWT:ValidIssuer"],
            audience: _configuration["JWT:ValidAudience"],
            expires: DateTime.Now.AddHours(1),
            claims: authClaims,
            signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
        );

        var token = new JwtSecurityTokenHandler().WriteToken(result);

        return token;
    }
}