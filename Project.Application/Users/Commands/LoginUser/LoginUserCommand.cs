using MediatR;

namespace Project.Application.Users.Commands.LoginUser;

public sealed class LoginUserCommand : IRequest<string>
{
    public string PhoneNumber { get; set; }
    public string Password { get; set; }
    
    public LoginUserCommand(string phoneNumber, string password)
    {
        PhoneNumber = phoneNumber;
        Password = password;
    }
}