using FluentValidation;

namespace Project.Application.Users.Commands.LoginUser;

public class LoginUserCommandValidator : AbstractValidator<LoginUserCommand>
{
    public LoginUserCommandValidator()
    {
        RuleFor(o => o.PhoneNumber).NotEmpty();
        RuleFor(o => o.Password).NotEmpty();
    }
}