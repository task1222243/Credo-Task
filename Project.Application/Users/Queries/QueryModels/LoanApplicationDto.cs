using Project.Domain.Enums;

namespace Project.Application.Users.Queries.QueryModels;

public record LoanApplicationDto(
    int Id,
    decimal Amount,
    LoanType LoanType,
    LoanCurrency LoanCurrency,
    LoanStatus LoanStatus,
    int LoanPeriodInMonth);