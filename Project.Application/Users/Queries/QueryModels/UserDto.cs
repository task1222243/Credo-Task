namespace Project.Application.Users.Queries.QueryModels;

public record UserDto(
    int Id,
    string FirstName,
    string LastName,
    string PhoneNumber,
    string PersonalNumber, 
    DateOnly DateOfBirth, 
    DateTime RegistrationDate, 
    List<LoanApplicationDto> LoanApplicationList);