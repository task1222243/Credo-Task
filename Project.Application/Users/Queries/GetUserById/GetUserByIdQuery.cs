using MediatR;
using Microsoft.Extensions.Logging;
using Project.Application.Common.Extensions;
using Project.Application.Exceptions;
using Project.Application.Interfaces;
using Project.Application.Users.Queries.QueryModels;

namespace Project.Application.Users.Queries.GetUserById;

[Serializable]
public sealed class GetUserByIdQuery : IRequest<UserDto>
{
    public int UserId { get; set; }
}

public sealed class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, UserDto>
{
    private readonly IUserRepository _userRepository;
    private readonly ILogger<GetUserByIdQueryHandler> _logger;

    public GetUserByIdQueryHandler(
        IUserRepository userRepository,
        ILogger<GetUserByIdQueryHandler> logger)
    {
        _userRepository = userRepository;
        _logger = logger;
    }

    public async Task<UserDto> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _userRepository.GetUserByIdAsync(request.UserId);

            if (user is null)
            {
                throw new NotFoundException("User not found");
            }

            return user.ToUserDto();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
    }
}