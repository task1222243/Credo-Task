using FluentValidation;

namespace Project.Application.LoanApplications.Queries.GetLoanApplicationsByUserId;

public sealed class GetLoanApplicationsQueryValidator : AbstractValidator<GetLoanApplicationsByUserIdQuery>
{
    public GetLoanApplicationsQueryValidator()
    {
        RuleFor(o => o.UserId).NotEmpty().WithMessage("User Id must not be empty");
    }
}