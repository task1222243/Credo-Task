using MediatR;
using Microsoft.Extensions.Logging;
using Project.Application.Common.Extensions;
using Project.Application.Exceptions;
using Project.Application.Interfaces;
using Project.Application.Users.Queries.QueryModels;

namespace Project.Application.LoanApplications.Queries.GetLoanApplicationsByUserId;

[Serializable]
public sealed class GetLoanApplicationsByUserIdQuery : IRequest<List<LoanApplicationDto>>
{
    public int UserId { get; set; }
}

public sealed class
    GetLoanApplicationsByUserIdQueryHandler : IRequestHandler<GetLoanApplicationsByUserIdQuery,
        List<LoanApplicationDto>>
{
    private readonly IUserRepository _userRepository;
    private readonly ILoanApplicationHubRepository _loanApplicationHubRepository;
    private readonly ILogger<GetLoanApplicationsByUserIdQueryHandler> _logger;

    public GetLoanApplicationsByUserIdQueryHandler(
        IUserRepository userRepository,
        ILoanApplicationHubRepository loanApplicationHubRepository,
        ILogger<GetLoanApplicationsByUserIdQueryHandler> logger)
    {
        _userRepository = userRepository;
        _loanApplicationHubRepository = loanApplicationHubRepository;
        _logger = logger;
    }

    public async Task<List<LoanApplicationDto>> Handle(GetLoanApplicationsByUserIdQuery request,
        CancellationToken cancellationToken)
    {
        try
        {
            var user = await _userRepository.GetUserByIdAsync(request.UserId);

            if (user is null)
            {
                throw new NotFoundException("User not found");
            }

            return (await _loanApplicationHubRepository.GetLoanApplicationsByUserId(request.UserId))
                .Select(o => o.ToLoanApplicationDto()).ToList();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
    }
}