using MediatR;
using Microsoft.Extensions.Logging;
using Project.Application.Exceptions;
using Project.Application.Interfaces;
using Project.Domain.Entities;

namespace Project.Application.LoanApplications.Commands.CreateLoanApplication;

public sealed class CreateLoanApplicationCommandHandler : IRequestHandler<CreateLoanApplicationCommand, int>
{
    private readonly IUserRepository _userRepository;
    private readonly ILogger<CreateLoanApplicationCommandHandler> _logger;

    public CreateLoanApplicationCommandHandler(
        IUserRepository userRepository,
        ILogger<CreateLoanApplicationCommandHandler> logger)
    {
        _userRepository = userRepository;
        _logger = logger;
    }

    public async Task<int> Handle(CreateLoanApplicationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _userRepository.GetUserByIdAsync(request.UserId);

            if (user is null)
            {
                throw new NotFoundException($"User not found {request.UserId}");
            }

            var loanApplication = new LoanApplication(
                request.UserId,
                request.Amount,
                request.LoanType,
                request.LoanCurrency,
                request.LoanPeriodInMonth);
    
            user.AddLoanApplication(loanApplication);

            await _userRepository.UpdateUserAsync(user);

            return loanApplication.Id;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
    }
}