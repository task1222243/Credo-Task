using FluentValidation;

namespace Project.Application.LoanApplications.Commands.CreateLoanApplication;

public sealed class CreateLoanApplicationCommandValidator : AbstractValidator<CreateLoanApplicationCommand>
{
    public CreateLoanApplicationCommandValidator()
    {
        RuleFor(o => o.UserId).NotEmpty();
        RuleFor(o => o.LoanCurrency).IsInEnum();
        RuleFor(o => o.LoanType).IsInEnum();
        RuleFor(o => o.Amount)
            .GreaterThan(0)
            .WithMessage("Amount should be greater than zero.");
        RuleFor(o => o.LoanPeriodInMonth).NotEmpty();
    }
}