using MediatR;
using Project.Domain.Enums;

namespace Project.Application.LoanApplications.Commands.CreateLoanApplication;

[Serializable]
public sealed class CreateLoanApplicationCommand : IRequest<int>
{
    public int UserId { get; set; }
    public LoanType LoanType { get; set; }
    public LoanCurrency LoanCurrency { get; set; }
    public decimal Amount { get; set; }
    public int LoanPeriodInMonth { get; set; }

    public CreateLoanApplicationCommand(
        int userId,
        LoanType loanType,
        LoanCurrency loanCurrency,
        decimal amount,
        int loanPeriodInMonth)
    {
        UserId = userId;
        LoanType = loanType;
        LoanCurrency = loanCurrency;
        Amount = amount;
        LoanPeriodInMonth = loanPeriodInMonth;
    }
}