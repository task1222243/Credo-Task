using MediatR;

namespace Project.Application.LoanApplications.Commands.DeleteLoanApplication;

[Serializable]
public sealed class DeleteLoanApplicationCommand : IRequest<int>
{
    public int UserId { get; set; }
    public int LoanId { get; set; }
    
    public DeleteLoanApplicationCommand(int userId, int loanId)
    {
        UserId = userId;
        LoanId = loanId;
    }
}