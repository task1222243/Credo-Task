using MediatR;
using Microsoft.Extensions.Logging;
using Project.Application.Exceptions;
using Project.Application.Interfaces;

namespace Project.Application.LoanApplications.Commands.DeleteLoanApplication;

public sealed class DeleteLoanApplicationCommandHandler : IRequestHandler<DeleteLoanApplicationCommand, int>
{
    private readonly IUserRepository _userRepository;
    private readonly ILoanApplicationHubRepository _loanApplicationHubRepository;
    private readonly ILogger<DeleteLoanApplicationCommandHandler> _logger;

    public DeleteLoanApplicationCommandHandler(
        IUserRepository userRepository,
        ILoanApplicationHubRepository loanApplicationHubRepository,
        ILogger<DeleteLoanApplicationCommandHandler> logger)
    {
        _userRepository = userRepository;
        _loanApplicationHubRepository = loanApplicationHubRepository;
        _logger = logger;
    }

    public async Task<int> Handle(DeleteLoanApplicationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _userRepository.GetUserByIdAsync(request.UserId);

            if (user is null)
            {
                throw new NotFoundException($"User not found {request.UserId}");
            }

            var loanApplicationToRemove = await _loanApplicationHubRepository
                .GetLoanApplicationByLoanId(request.LoanId);

            if (loanApplicationToRemove is null)
            {
                throw new NotFoundException($"Application on this id not found {request.LoanId}");
            }

            if (user.Loans is null)
            {
                throw new NotFoundException($"User on this id {request.UserId} has no applications");
            }

            user.Loans!.Remove(loanApplicationToRemove);

            await _loanApplicationHubRepository.DeleteLoanApplication(loanApplicationToRemove);

            await _userRepository.UpdateUserAsync(user);

            return request.UserId;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
    }
}