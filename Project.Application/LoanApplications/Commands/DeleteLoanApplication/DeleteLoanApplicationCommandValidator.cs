using FluentValidation;

namespace Project.Application.LoanApplications.Commands.DeleteLoanApplication;

public sealed class DeleteLoanApplicationCommandValidator : AbstractValidator<DeleteLoanApplicationCommand>
{
    public DeleteLoanApplicationCommandValidator()
    {
        RuleFor(o => o.UserId);
        RuleFor(o => o.LoanId);
    }
}