using MediatR;
using Project.Domain.Enums;

namespace Project.Application.LoanApplications.Commands.UpdateLoanApplication;

public sealed class UpdateLoanApplicationCommand : IRequest
{
    public int UserId { get; set; }
    public int LoanId { get; set; }
    public decimal Amount { get; set; }
    public LoanType LoanType { get; set; }
    public LoanCurrency LoanCurrency { get; set; }
}