using FluentValidation;

namespace Project.Application.LoanApplications.Commands.UpdateLoanApplication;

public sealed class UpdateLoanApplicationCommandValidator : AbstractValidator<UpdateLoanApplicationCommand>
{
    public UpdateLoanApplicationCommandValidator()
    {
        RuleFor(o => o.LoanId).NotEmpty();
        RuleFor(o => o.UserId).NotEmpty();
    }
}