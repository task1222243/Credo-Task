using MediatR;
using Microsoft.Extensions.Logging;
using Project.Application.Exceptions;
using Project.Application.Interfaces;
using Project.Domain.Enums;

namespace Project.Application.LoanApplications.Commands.UpdateLoanApplication;

public sealed class UpdateLoanApplicationCommandHandler : IRequestHandler<UpdateLoanApplicationCommand>
{
    private readonly IUserRepository _userRepository;
    private readonly ILoanApplicationHubRepository _loanApplicationRepository;
    private readonly ILogger<UpdateLoanApplicationCommandHandler> _logger;

    public UpdateLoanApplicationCommandHandler(
        IUserRepository userRepository,
        ILoanApplicationHubRepository loanApplicationRepository, 
        ILogger<UpdateLoanApplicationCommandHandler> logger)
    {
        _userRepository = userRepository;
        _loanApplicationRepository = loanApplicationRepository;
        _logger = logger;
    }

    public async Task<Unit> Handle(UpdateLoanApplicationCommand request, CancellationToken cancellationToken)
    {

        try
        {
            var user = await _userRepository.GetUserByIdAsync(request.UserId);

            if (user is null)
            {
                throw new NotFoundException($"User  on this id {request.UserId} not found");
            }

            var loanToUpdate = await _loanApplicationRepository.GetLoanApplicationByLoanId(request.LoanId);

            if (loanToUpdate is null)
            {
                throw new NotFoundException($"loan application on this id {request.LoanId} not found");
            }

            if (loanToUpdate.LoanStatus is LoanStatus.Approved or LoanStatus.Rejected)
            {   
                throw new BadRequestException(
                    $"You cannot update an existing loan in this {loanToUpdate.LoanStatus} status");
            }

            loanToUpdate.UpdateLoanApplication(request.Amount, request.LoanType, request.LoanCurrency);

            await _loanApplicationRepository.UpdateLoanApplication(loanToUpdate);

            return Unit.Value;

        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
        
    }
}