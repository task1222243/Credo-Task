namespace Project.Application.Exceptions;

public class BadRequestException : Exception
{
    // ReSharper disable once UnusedMember.Global
    public BadRequestException()
    {
    }

    public BadRequestException(string message) 
        : base(message)
    {
    }
}