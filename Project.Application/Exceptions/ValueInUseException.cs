namespace Project.Application.Exceptions;

public class ValueInUseException : Exception
{
    public ValueInUseException()
    {
    }

    public ValueInUseException(string message)
        : base(message)
    {
    }

    public ValueInUseException(string message, Exception innerException)
        : base(message, innerException)
    {
    }

    public ValueInUseException(string name, object key)
        : base($"Name : {name}, Value : {key}")
    {
    }
}