using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Project.Application.Interfaces;
using Project.Infrastructure.Persistence;
using Project.Infrastructure.Repositories;
using Project.Infrastructure.Services;

namespace Project.Infrastructure;

public static class ConfigureServices
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
    {
            services.AddDbContext<CredoDbContext>(options =>
                options.UseInMemoryDatabase("CleanArchitectureDb"));
        
        
        services.AddScoped<ICustomPasswordHasher, CustomPasswordHasher>();
        services.AddScoped<ICustomPasswordValidator, CustomPasswordValidator>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<ILoanApplicationHubRepository, LoanApplicationHubRepository>();
        

        return services;
    }
}