using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.Domain.Entities;

namespace Project.Infrastructure.Persistence.Configurations;

public sealed class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasKey(user => user.Id);

        builder.Property(o => o.FirstName).IsRequired();
        builder.Property(o => o.LastName).IsRequired();
        builder.Property(o => o.PersonalNumber).IsRequired();
        builder.Property(o => o.DateOfBirth).IsRequired();

        builder.HasIndex(o => o.PersonalNumber).IsUnique();
        builder.HasIndex(o => new { o.FirstName, o.LastName });

        builder.HasMany(user => user.Loans)
            .WithOne(loan => loan.User)
            .HasForeignKey(loan => loan.UserId);
    }
}