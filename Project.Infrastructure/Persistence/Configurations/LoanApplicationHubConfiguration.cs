using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.Domain.Entities;

namespace Project.Infrastructure.Persistence.Configurations;

public sealed class LoanApplicationHubConfiguration : IEntityTypeConfiguration<LoanApplication>
{
    public void Configure(EntityTypeBuilder<LoanApplication> builder)
    {
       builder.HasKey(o => o.Id);

        builder.Property(o => o.Id).ValueGeneratedOnAdd();

        builder.HasIndex(o => o.Id);
        /*1. comandebis gadatana 2. logebis gasworeba abstraquli ILogger<commandhandler>3.primary keys isedac aq mgoni
         indexi naxe4. ricxvebi sheamowme loani minus amount arunda iyos*/
    }
}