using Microsoft.EntityFrameworkCore;
using Project.Application.Interfaces;
using Project.Domain.Entities;

namespace Project.Infrastructure.Persistence;

public class CredoDbContext : DbContext, IApplicationDbContext
{
    public CredoDbContext(DbContextOptions<CredoDbContext> options) : base(options)
    {
    }

    public DbSet<User> Users => Set<User>();

    public DbSet<LoanApplication> LoanApplicationHub => Set<LoanApplication>();
}