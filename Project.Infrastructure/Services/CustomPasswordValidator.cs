using Microsoft.AspNetCore.Identity;
using Project.Application.Interfaces;

namespace Project.Infrastructure.Services;

public class CustomPasswordValidator : ICustomPasswordValidator
{
    public Task<IdentityResult> ValidateAsync(string password)
    {
        if (!password.Any(char.IsLower))
        {
            return Task.FromResult(IdentityResult.Failed(new IdentityError
            {
                Code = "PasswordRequiresLower",
                Description = "Password must contain at least one lowercase letter."
            }));
        }

        if (!password.Any(char.IsUpper))
        {
            return Task.FromResult(IdentityResult.Failed(new IdentityError
            {
                Code = "PasswordRequiresUpper",
                Description = "Password must contain at least one uppercase letter."
            }));
        }

        if (!password.Any(char.IsDigit))
        {
            return Task.FromResult(IdentityResult.Failed(new IdentityError
            {
                Code = "PasswordRequiresDigit",
                Description = "Password must contain at least one digit."
            }));
        }

        if (password.Length < 8)
        {
            return Task.FromResult(IdentityResult.Failed(new IdentityError
            {
                Code = "PasswordRequiresMinimumLength",
                Description = "Password length must be greater or equal 8"
            }));
        }

        return Task.FromResult(IdentityResult.Success);
    }
}