using Microsoft.EntityFrameworkCore;
using Project.Application.Interfaces;
using Project.Domain.Entities;
using Project.Infrastructure.Persistence;

namespace Project.Infrastructure.Repositories;

public sealed class LoanApplicationHubRepository : ILoanApplicationHubRepository
{
    private readonly CredoDbContext _context;

    public LoanApplicationHubRepository(CredoDbContext context)
    {
        _context = context;
    }

    public async Task<int> CreateLoanApplicationAsync(LoanApplication application)
    {
        await _context.LoanApplicationHub.AddAsync(application);

        await _context.SaveChangesAsync();

        return application.Id;
    }

    public async Task<List<LoanApplication>> GetLoanApplicationsByUserId(int userId)
    {
        return await _context.LoanApplicationHub.Where(o => o.UserId == userId).ToListAsync();
    }

    public async Task<LoanApplication?> GetLoanApplicationByLoanId(int loanId)
    {
        return await _context.LoanApplicationHub.FirstOrDefaultAsync(o => o.Id == loanId);
    }

    public async Task<LoanApplication> UpdateLoanApplication(LoanApplication loanApplication)
    {
        _context.LoanApplicationHub.Update(loanApplication);

        await _context.SaveChangesAsync();

        return loanApplication;
    }

    public async Task<int> DeleteLoanApplication(LoanApplication loanApplication)
    {
        _context.LoanApplicationHub.Remove(loanApplication);

        await _context.SaveChangesAsync();

        return loanApplication.Id;
    }
}