using Microsoft.EntityFrameworkCore;
using Project.Application.Interfaces;
using Project.Domain.Entities;
using Project.Infrastructure.Persistence;

namespace Project.Infrastructure.Repositories;

public sealed class UserRepository : IUserRepository
{
    private readonly CredoDbContext _context;

    public UserRepository(CredoDbContext context)
    {
        _context = context;
    }

    public async Task<User?> GetUserByIdAsync(int userId)
    {
        return await _context.Users
            .Include(u => u.Loans)
            .FirstOrDefaultAsync(o => o.Id == userId);
    }

    public async Task<int> AddUserAsync(User user)
    {
        await _context.Users.AddAsync(user);

        await _context.SaveChangesAsync();

        return user.Id;
    }

    public async Task<User> UpdateUserAsync(User user)
    {
        _context.Users.Update(user);

        await _context.SaveChangesAsync();

        return user;
    }

    public async Task<User?> GetUserByPhoneNumber(string phoneNumber)
    {
        return await _context.Users.FirstOrDefaultAsync(o => o.PhoneNumber == phoneNumber) ?? null;
    }

    public Task<List<User>> GetUsers(Func<User?, bool> filter)
    {
        return Task.FromResult(_context.Users.Where(filter).ToList());
    }
}