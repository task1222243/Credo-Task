using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project.Api.ApiOptions;
using Project.Application.Users.Commands.CreateUser;
using Project.Application.Users.Commands.LoginUser;
using Project.Application.Users.Queries.GetUserById;
using Project.Application.Users.Queries.QueryModels;

namespace Project.Api.Controllers;

[Route("v{version:apiVersion}/[controller]")]
[ApiVersion(ApiVersions.V1)]
public class UserController : ApiControllerBase
{
    
    [HttpPost("create-user")]
    [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(ValidationProblemDetails), (int)HttpStatusCode.UnprocessableEntity)]
    [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
    public async Task<int> CreateUser(CreateUserCommand request) => await Mediator.Send(request);


    [Authorize]
    [HttpGet("get-user-by-id")]
    [ProducesResponseType(typeof(UserDto), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
    public async Task<UserDto> GetUserById([FromQuery] GetUserByIdQuery request) => await Mediator.Send(request);

    [HttpPost("login-user")]
    [ProducesResponseType(typeof(ValidationProblemDetails), (int)HttpStatusCode.UnprocessableEntity)]
    [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
    public async Task<string> LoginUser(LoginUserCommand request) => await Mediator.Send(request);
}