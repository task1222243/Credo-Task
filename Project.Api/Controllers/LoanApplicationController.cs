using System.Net;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project.Api.ApiOptions;
using Project.Application.LoanApplications.Commands.CreateLoanApplication;
using Project.Application.LoanApplications.Commands.DeleteLoanApplication;
using Project.Application.LoanApplications.Commands.UpdateLoanApplication;
using Project.Application.LoanApplications.Queries.GetLoanApplicationsByUserId;
using Project.Application.Users.Queries.QueryModels;

namespace Project.Api.Controllers;

[Authorize]
[Route("v{version:apiVersion}/[controller]")]
[ApiVersion(ApiVersions.V1)]
public class LoanApplicationController : ApiControllerBase
{
    [HttpPost("create-loan-application")]
    [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
    public async Task<int> CreateLoanApplication(CreateLoanApplicationCommand request) =>
        await Mediator.Send(request);

    [HttpGet("get-loan-applications-by-user-id")]
    [ProducesResponseType(typeof(List<LoanApplicationDto>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
    public async Task<List<LoanApplicationDto>>
        GetLoanApplicationsByUserId([FromQuery] GetLoanApplicationsByUserIdQuery request) =>
        await Mediator.Send(request);

    [HttpPut("update-loan-application")]
    [ProducesResponseType(typeof(Unit), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
    public async Task<Unit> UpdateLoanApplication(UpdateLoanApplicationCommand request) => await Mediator.Send(request);
    
    [HttpPost("delete-loan-application")]
    [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
    public async Task<int> DeleteLoanApplication(DeleteLoanApplicationCommand request) => await Mediator.Send(request);
    
    
    
}