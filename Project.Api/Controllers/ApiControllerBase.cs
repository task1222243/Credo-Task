using MediatR;
using Microsoft.AspNetCore.Mvc;
using Project.Api.Filters;

namespace Project.Api.Controllers;

[ApiController]
[ApiExceptionFilter]

public abstract class ApiControllerBase : ControllerBase
{
    private ISender? _mediator;
    protected ISender Mediator => _mediator ??= HttpContext.RequestServices.GetRequiredService<ISender>();
}