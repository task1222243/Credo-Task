namespace Project.Api.ApiOptions;

[Serializable]
public class ApiVersions
{
    public const string V1 = "1.0";

    public const string Latest = V1;

    public const string ApiVersion = nameof(V1);
}