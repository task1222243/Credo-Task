პროექტი საშუალებას იძლევა მომხმარებელმა გაიაროს რეგისტრაცია. რეგისტრაციის შემდეგ დაავტორიზდეს, რის შემდეგაც შეეძლება გააგეთოს განაცხადი სესხის თაობაზე ასევე ნახოს თავისი გაკეთბული განცხადებების სია და დაარედაქტიროს ისინი.

სვაგერში ჩაშენებულია JWT ავტორიზაცია. Authorize ღილაკის დაჭერის შემდეგ, input_ში ჩაწერეთ : Bearer თქვენი ტოკენი რომელიც დაგიბრუნათ /v1/User/login-user ენდფოინთმა.

/v1/User/create-user - გამოიყენება მომხმარებლის შესაქმნელად. აბრუნებს მომხარებლის ID_ს.
{
"firstName": "string",
"lastName": "string",
"phoneNumber": "string",
"personalNumber": "string",
"password": "string",
"dateOfBirth": "2023-10-29"
}

/v1/User/login-user - ავტორიზაცია, აბრუნებს JWT_ტოკენს.
{
"phoneNumber": "string",
"password": "string"
}

/v1/User/get-user-by-id მომხმარებლის ნახვა ID_ის მეშვეობით. იღებს პარამეტრად მომხმარებლის ID_ს

/v1/LoanApplication/create-loan-application სესხის განაცხადის გადაგზავნა.
{
"userId": 0,
"loanType": "AutoLoan",
"loanCurrency": "Gel",
"amount": 0,
"loanPeriodInMonth": 0
}

/v1/LoanApplication/update-loan-application სესხის განაცხადის რედაქტირება. არ რედაქტირდება მხოლოდ სტატუსი.
{
"userId": 0,
"loanId": 0,
"amount": 0,
"loanType": "AutoLoan",
"loanCurrency": "Gel"
}

/v1/LoanApplication/get-loan-applications-by-user-id სესხის განცხადებების ნახვა მომხმარებლის ID_ით, აბრუნებს მომხმარებლის განცხადებების კოლექციას.

/v1/LoanApplication/delete-loan-application სესხის განაცხადის წაშლა.
{
"userId": 0,
"loanId": 0
}
